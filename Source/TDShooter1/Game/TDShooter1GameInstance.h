// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TDShooter1/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TDShooter1/CastSystem/WeaponDefault.h"
#include "TDShooter1GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TDSHOOTER1_API UTDShooter1GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ")
		UDataTable* WeaponInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
};
