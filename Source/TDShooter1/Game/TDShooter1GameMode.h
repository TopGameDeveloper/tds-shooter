// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDShooter1GameMode.generated.h"

UCLASS(minimalapi)
class ATDShooter1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDShooter1GameMode();
};



