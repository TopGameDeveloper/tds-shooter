// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShooter1GameMode.h"
#include "TDShooter1PlayerController.h"
#include "TDShooter1/Character/TDShooter1Character.h"
#include "UObject/ConstructorHelpers.h"

ATDShooter1GameMode::ATDShooter1GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDShooter1PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
