// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShooter1PlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "TDShooter1/Character/TDShooter1Character.h"
#include "Engine/World.h"

ATDShooter1PlayerController::ATDShooter1PlayerController()
{
	/*bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;*/
}

void ATDShooter1PlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	
}

void ATDShooter1PlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	//InputComponent->BindAction("SetDestination", IE_Pressed, this, &ATDShooter1PlayerController::OnSetDestinationPressed);
	//InputComponent->BindAction("SetDestination", IE_Released, this, &ATDShooter1PlayerController::OnSetDestinationReleased);

	//// support touch devices 
	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATDShooter1PlayerController::MoveToTouchLocation);
	//InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ATDShooter1PlayerController::MoveToTouchLocation);

	//InputComponent->BindAction("ResetVR", IE_Pressed, this, &ATDShooter1PlayerController::OnResetVR);
}

void ATDShooter1PlayerController::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ATDShooter1PlayerController::MoveToMouseCursor()
{
	//if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	//{
	//	if (ATDShooter1Character* MyPawn = Cast<ATDShooter1Character>(GetPawn()))
	//	{
	//		if (MyPawn->GetCursorToWorld())
	//		{
	//			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
	//		}
	//	}
	//}
	//else
	//{
	//	// Trace to see what is under the mouse cursor
	//	FHitResult Hit;
	//	GetHitResultUnderCursor(ECC_Visibility, false, Hit);

	//	if (Hit.bBlockingHit)
	//	{
	//		// We hit something, move there
	//		SetNewMoveDestination(Hit.ImpactPoint);
	//	}
	//}
}

void ATDShooter1PlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	//FVector2D ScreenSpaceLocation(Location);

	//// Trace to see what is under the touch location
	//FHitResult HitResult;
	//GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	//if (HitResult.bBlockingHit)
	//{
	//	// We hit something, move there
	//	SetNewMoveDestination(HitResult.ImpactPoint);
	//}
}

void ATDShooter1PlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	//APawn* const MyPawn = GetPawn();
	//if (MyPawn)
	//{
	//	float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

	//	// We need to issue move command only if far enough in order for walk animation to play correctly
	//	if ((Distance > 120.0f))
	//	{
	//		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
	//	}
	//}
}

void ATDShooter1PlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	//bMoveToMouseCursor = true;
}

void ATDShooter1PlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	//bMoveToMouseCursor = false;
}
