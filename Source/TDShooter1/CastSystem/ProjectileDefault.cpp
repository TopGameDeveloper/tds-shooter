// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpellCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	SpellCollisionSphere->SetSphereRadius(60.f);

	SpellCollisionSphere->bReturnMaterialOnMove = true;
	SpellCollisionSphere->SetCanEverAffectNavigation(false);

	RootComponent = SpellCollisionSphere;
	

	SpellProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Spell ProjectileMovement"));
	SpellProjectileMovement->UpdatedComponent = RootComponent;
	SpellProjectileMovement->InitialSpeed = ProjectileSetting.ProjectileSpeed;
	//SpellProjectileMovement -> SetupAttachment(RootComponent);
	SpellProjectileMovement->MaxSpeed = 0.f;

	SpellFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Spell FX"));
	SpellFX->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();
	SpellCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::SpellCollisionSphereBeginOverlap);
	SpellCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::SpellCollisionSphereHit);
	SpellCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::SpellCollisionSphereEndOverlap);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//Move();
}

void AProjectileDefault::SpellCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		const EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);
		if (ProjectileSetting.HitDecals.Contains(MySurfaceType))
		{
			UMaterialInterface* myMaterial = ProjectileSetting.HitDecals[MySurfaceType];
			if (myMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);

			}
		}
		if (ProjectileSetting.HitFXs.Contains(MySurfaceType))
		{
			UParticleSystem* myParticle = ProjectileSetting.HitFXs[MySurfaceType];
			if (myParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(0.5,0.5,0.5)));
			}

		}
		if (ProjectileSetting.HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.HitSound, Hit.ImpactPoint);
		}

	}
	UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
	ImpactProjectile();
}

void AProjectileDefault::SpellCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	
}

void AProjectileDefault::SpellCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}
void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	SpellProjectileMovement->InitialSpeed = InitParam.ProjectileSpeed;
	SpellProjectileMovement->MaxSpeed = InitParam.ProjectileSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);
	SpellProjectileMovement->ProjectileGravityScale = InitParam.Gravity;
	if(InitParam.SpellFX)
	{
		SpellFX->SetTemplate(InitParam.SpellFX);
	}
	else
		SpellFX->DestroyComponent();
	ProjectileSetting = InitParam;
}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}
