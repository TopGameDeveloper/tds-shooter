// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"

//#include "../../../../UE_4.27/Engine/Shaders/Private/PathTracing/Light/PathTracingLightCommon.ush"
#include "Kismet/GameplayStatics.h"
// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SphereFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("SphereFX"));
	SphereFX->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SpellTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!SpellCasting)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
	}
}

void AWeaponDefault::SpellTick(float DeltaTime)
{
	if (GetWeaponRound() > 0)
	{
		if (SpellCasting)
			if (FireTimer < 0.f)
			{

				if (!WeaponReloading)
					Fire();
			}
			else
				FireTimer -= DeltaTime;
	}
	else
	{

		if (!WeaponReloading)
		{
			InitReload();
		}
	}
		
		
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		SpellCasting = bIsFire;
	else
	{
		SpellCasting = false;
		FireTimer = 0.01f;
	}
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

void AWeaponDefault::WeaponInit(FWeaponInfo WeaponInfo1)
{
	UpdateStateWeapon(EMovementState::Run_State);
	WeaponSetting = WeaponInfo1;
	WeaponInfo.Round = WeaponSetting.MaxRound;
	SphereFX->SetRelativeScale3D(WeaponSetting.ProjectileSetting.ProjectileScale);
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfCast;
	ChangeDispersionByShot();
	OnWeaponFire.Broadcast(WeaponSetting.AnimationInfo.AnimCharCast);
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundCastSpell, ShootLocation->GetComponentLocation());
	int8 NumberProjectile = GetNumberProjectileByShot();
	WeaponInfo.Round = WeaponInfo.Round - 1;
	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();
		FVector EndLocation;

		for (int8 i = 0; i < NumberProjectile; i++)
		{
			
			if (ProjectileInfo.Projectile)
			{
				//Init Cast
				EndLocation = GetFireEndLocation();
				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();
				FMatrix MyMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = MyMatrix.Rotator();
				FVector oldScale=SphereFX->GetComponentScale();
				FVector NewScale= oldScale*(1-(1/WeaponSetting.MaxRound)*0.8);
				SphereFX->SetWorldScale3D(NewScale);
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				// FHitResult Hit;
				// TArray<AActor*> Actors;
				// UKismetSystemLibrary::LineTraceSingle(GetWorld(),SpawnLocation,EndLocation*WeaponSetting.DistanceTrace,ETraceTypeQuery::TraceTypeQuery4,false,Actors,EDrawDebugTrace::ForDuration,Hit,true,
				// 	FLinearColor::Red,FLinearColor::Green, 5.0f);
				// if(Hit.GetActor()&& Hit.PhysMaterial.IsValid())
				// {
				// 	EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);
				// 	if (WeaponSetting.ProjectileSetting.HitDecals.Contains(MySurfaceType))
				// 	{
				// 		UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[MySurfaceType];
				//
				// 		
				// 		if (myMaterial && Hit.GetComponent())
				// 		{
				// 			UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
				//
				// 		}
				// 	}
				// 		
				// 	if (WeaponSetting.ProjectileSetting.HitFXs.Contains(MySurfaceType))
				// 	{
				// 		UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[MySurfaceType];
				// 		if (myParticle)
				// 		{
				// 			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(0.5,0.5,0.5)));
				// 		}
				// 	}
				//
				// 		
				// 	if (WeaponSetting.ProjectileSetting.HitSound)
				// 	{
				// 		UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
				// 	}
				// 	UGameplayStatics::ApplyDamage(Hit.GetActor(),WeaponSetting.ProjectileSetting.ProjectileDamage,GetInstigatorController(),this,NULL);
				// }
				
			}
		}
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;
	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::SprintRun_State:
		BlockFire = true;
		SetWeaponStateFire(false);
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
}

int32 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

FVector AWeaponDefault::GetFireEndLocation() 
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.0f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - EndShootLocation);
	UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());
	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() -EndShootLocation).GetSafeNormal()) * -20000.0f;
		//UE_LOG(LogTemp, Warning, TEXT("true"));
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		//UE_LOG(LogTemp, Warning, TEXT("false"));
	}
	

	return EndLocation;
}


int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	ReloadTimer = WeaponSetting.ReloadTime;
	ReloadTime = WeaponSetting.ReloadTime;
	if(WeaponSetting.AnimationInfo.AnimCharReload)
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimationInfo.AnimCharReload);

}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0)
		{
			FinishReload();
			
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}

	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	WeaponInfo.Round = WeaponSetting.MaxRound;
	SphereFX->SetVisibility(true);
	SphereFX->SetRelativeScale3D(WeaponSetting.ProjectileSetting.ProjectileScale);
	OnWeaponReloadEnd.Broadcast();

}

void AWeaponDefault::InitDestroy()//RecastSpellSocket
{
	UE_LOG(LogTemp, Warning, TEXT("InitDestroy"));
	if (WeaponSetting.ProjectileSetting.ExploseFX)
	{
	
		UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.ExploseFX;
	//	FVector SpawnLocation = FVector((this->GetActorLocation().X-7.794329),(this->GetActorLocation().Y-108.140885),(this->GetActorLocation().Z-22.201042));
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(this->GetActorRotation(), this->GetActorLocation(), FVector(0.2,0.2,0.2)));
		SphereFX->SetVisibility(false);
	}
}
