// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "TDShooter1/FuncLibrary/Types.h"
#include "Particles/ParticleSystemComponent.h"
#include "TDShooter1/CastSystem/ProjectileDefault.h"
#include "WeaponDefault.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFire, UAnimMontage*, Anim);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireEnd);
UCLASS()
class TDSHOOTER1_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	FOnWeaponFire OnWeaponFire;
	//FOnWeaponFireEnd OnWeaponFireEnd;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UParticleSystemComponent* SphereFX = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
		FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FAddicionalWeaponInfo WeaponInfo;

	FVector EndShootLocation = FVector(0);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void DispersionTick(float DeltaTime);
	void SpellTick(float DeltaTime);


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpellLogic")
		bool SpellCasting = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpellLogic")
		bool WeaponReloading = false;
	UFUNCTION(BlueprintCallable)
		void SetWeaponStateFire(bool bIsFire);

	bool CheckWeaponCanFire();
	void WeaponInit(FWeaponInfo WeaponInfo1);
	FProjectileInfo GetProjectile();
	
	void Fire();

	void UpdateStateWeapon(EMovementState NewMovementState);
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot)const;
	int32 GetNumberProjectileByShot() const;
	FVector GetFireEndLocation();
	//flags
	bool BlockFire = false;
	//Dispersion
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;


	UFUNCTION(BlueprintCallable)
		int32 GetWeaponRound();

	UFUNCTION(BlueprintCallable)
	void InitReload();

	
	void ReloadTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		void FinishReload();
	//Timers'flags
	float FireTimer = 0.0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		float ReloadTime = 0.0f;
	UFUNCTION(BlueprintCallable)
		void InitDestroy();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 210.0f;

};
