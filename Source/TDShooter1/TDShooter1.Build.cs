// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDShooter1 : ModuleRules
{
	public TDShooter1(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore","PhysicsCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
