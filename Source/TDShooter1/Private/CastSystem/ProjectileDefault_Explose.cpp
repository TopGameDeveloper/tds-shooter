// Fill out your copyright notice in the Description page of Project Settings.


#include "CastSystem/ProjectileDefault_Explose.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugExplodeShow=0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TPS.DebugSExlode"),
	DebugExplodeShow,
	TEXT("Draw debug for explode"),
	ECVF_Cheat);
void AProjectileDefault_Explose::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Explose::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Explose::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Explose::SpellCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::SpellCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Explose::ImpactProjectile()
{
	TimerEnabled = true;
}

void AProjectileDefault_Explose::Explose()
{
	if(DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(),GetActorLocation(),ProjectileSetting.ProjectileMinRadiusDamage,12,FColor::Green,false,12);
		DrawDebugSphere(GetWorld(),GetActorLocation(),ProjectileSetting.ProjectileMaxRadiusDamage,12,FColor::Green,false,12);
	}
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}
	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage * 0.2f,
		GetActorLocation(),
		1000.0f,
		2000.0f,
		5,
		NULL, IgnoredActor, nullptr, nullptr);
	this->Destroy();
}
