// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShooter1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDShooter1, "TDShooter1" );

DEFINE_LOG_CATEGORY(LogTDShooter1)
 