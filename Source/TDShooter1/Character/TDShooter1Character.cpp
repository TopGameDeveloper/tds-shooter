// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShooter1Character.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TDShooter1/Game/TDShooter1GameInstance.h"


ATDShooter1Character::ATDShooter1Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDShooter1Character::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);

	if (SprintRunEnabled && Stamina > 0 && !OutOfStamina)
	{
		Stamina -= 1;
		
	}
	else
	{
		//EnableInput(NULL);
		if (Stamina == 0)
		{
			OutOfStamina = true;
			SprintRunEnabled = false;
			ChangeMovementState();
		}
		if (Stamina < 100)
			Stamina += 1;
		if (Stamina >= 100)
			OutOfStamina = false;
	}

}

void ATDShooter1Character::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(InitWeaponName);
	CurrentWeaponName = InitWeaponName;
	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}


void ATDShooter1Character::MovementTick(float DeltaTime)
{

	if (!SprintRunEnabled)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
	}
	else
	{
		AddMovementInput(GetActorForwardVector());
	}
	
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
			//myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
			float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			AWeaponDefault* myWeapon = GetCurrentWeapon();
			if (myWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 140.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 140.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 140.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::SprintRun_State:
					break;
				case EMovementState::AimWalk_State:
					CurrentWeapon->ShouldReduceDispersion = true;
					Displacement = FVector(0.0f, 0.0f, 140.0f);
					break;
				default:
					break;
				}
				myWeapon->EndShootLocation = ResultHit.Location + Displacement;
			}
			myWeapon->EndShootLocation;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
		}
	
}


void ATDShooter1Character::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDShooter1Character::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDShooter1Character::InputAxisY);

	NewInputComponent->BindAction(TEXT("CastEvent"), EInputEvent::IE_Pressed, this, &ATDShooter1Character::InputAttackPressed);		
	NewInputComponent->BindAction(TEXT("CastEvent"), EInputEvent::IE_Released, this, &ATDShooter1Character::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("Reload"), EInputEvent::IE_Pressed, this, &ATDShooter1Character::InputTryReloadEvent);

}

void ATDShooter1Character::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDShooter1Character::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDShooter1Character::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDShooter1Character::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDShooter1Character::InputTryReloadEvent()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound)
		{
			CurrentWeapon->InitReload();
		}
	}
}

void ATDShooter1Character::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDShooter1Character::ChangeMovementState()
{
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
		MovementState = EMovementState::Run_State;
	else
	{
		if (SprintRunEnabled && !OutOfStamina)
		{
			WalkEnabled = false;
			AimEnabled = false;
			//DisableInput(NULL);
			MovementState = EMovementState::SprintRun_State;
		}
		else
		{
			//bMoveForward = true;
			//bMoveRight = true;
		}
		if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			MovementState = EMovementState::Walk_State;
		if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
			MovementState = EMovementState::Aim_State;
		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
			MovementState = EMovementState::AimWalk_State;
	}
	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATDShooter1Character::AttackCharEvent(bool bIsCasting)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsCasting);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

AWeaponDefault* ATDShooter1Character::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDShooter1Character::InitWeapon(FName IdWeapon)
{
	UTDShooter1GameInstance* myGI = Cast<UTDShooter1GameInstance>(GetGameInstance());
	FWeaponInfo nyWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeapon, nyWeaponInfo))
		{
			if (nyWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotatin = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(nyWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotatin, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("SpellCast"));
					CurrentWeapon = myWeapon;
					myWeapon->WeaponInit(nyWeaponInfo);
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDShooter1Character::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDShooter1Character::WeaponReloadEnd);
					myWeapon->OnWeaponFire.AddDynamic(this, &ATDShooter1Character::WeaponFire);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - Weapon not Found -NULL"));
		}
	}
	
}

void ATDShooter1Character::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDShooter1Character::WeaponReloadEnd()
{

	WeaponReloadEnd_BP();
}

void ATDShooter1Character::WeaponFire(UAnimMontage* Anim)
{
	WeaponFire_BP(Anim);
}

void ATDShooter1Character::WeaponFire_BP_Implementation(UAnimMontage* Anim)
{

}
void ATDShooter1Character::WeaponReloadEnd_BP_Implementation()
{
	//BP
}

void ATDShooter1Character::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//bp
}

UDecalComponent* ATDShooter1Character::GetCursorToWorld()
{
	return CurrentCursor;
}